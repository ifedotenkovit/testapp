//
//  ViewController.swift
//  testApp
//
//  Created by  Igor Fedotenkov on 20.04.2020.
//  Copyright © 2020  Igor Fedotenkov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var customPushButton: UIButton!
    @IBOutlet var customLabelText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customPushButton.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        customLabelText.accessibilityIdentifier = "itemLabel"
        // Do any additional setup after loading the view.
    }
    @objc func buttonClicked() {
        customLabelText.text = "Button Clicked"
    }
}

